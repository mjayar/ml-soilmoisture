from hydra import compose, initialize
from omegaconf import DictConfig


import argparse
import cv2 as cv
import joblib
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from datetime import datetime
from glob import glob
from prettytable import PrettyTable
from scipy import stats
from sklearn.metrics import (
    mean_absolute_error,
    root_mean_squared_error,
    r2_score,
    PredictionErrorDisplay
)

from utils.image import non_zero_pixels_percentage


def evaluate(config: DictConfig, args: argparse.Namespace):
    path = f"models/{args.model}_{args.feat}.sav"
    assert os.path.isfile(path), f"Model not found: {path}"
    assert os.path.isfile(config.data.raw), f"CSV not found: {config.data.raw}"

    data = pd.read_csv(config.data.raw)
    model = joblib.load(path)

    target = data.iloc[:, 0].values
    predictions = np.array([])

    time_start = datetime.now()
    imgs = glob(os.path.join(config.images.raw, "*.jpg"))

    for i, img_path in enumerate(imgs):
        img_filename, ext = os.path.splitext(os.path.basename(img_path))
        progress = round(i / len(imgs) * 100, 1)
        print(f"[{progress}%] Processing "
              f"{img_filename}...", end="\r", flush=True)

        img = cv.imread(img_path)
        img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        gray = np.mean(img_gray)

        img_light = cv.threshold(img_gray, int(config.process.threshold), int(
            config.process.max_threshold), cv.THRESH_BINARY)[1]
        if bool(config.process.dilate):
            img_light = cv.dilate(img_light, kernel=np.ones(
                (10, 10), np.uint8), iterations=1)
        light = non_zero_pixels_percentage(img_light)

        img_neg = cv.bitwise_not(img_gray)
        img_dark = cv.threshold(img_neg, int(config.process.threshold), int(
            config.process.max_threshold), cv.THRESH_BINARY)[1]
        if bool(config.process.dilate):
            img_dark = cv.dilate(img_dark, kernel=np.ones(
                (10, 10), np.uint8), iterations=1)
        dark = non_zero_pixels_percentage(img_dark)

        hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
        hue, sat, val = cv.mean(hsv)[0:3]

        if args.feat == "gsv":
            features = np.array([gray])
        elif args.feat == "ltv":
            features = np.array([light])
        elif args.feat == "dtv":
            features = np.array([dark])
        elif args.feat == "hsv":
            features = np.array([light, dark, hue, sat, val])
        else:
            features = np.array([gray, light, dark])

        prediction = model.predict(np.array([features]))[0]
        predictions = np.append(predictions, prediction)

    time_end = datetime.now()
    print(f"[100%] Elapsed time: {time_end-time_start}")

    output = pd.DataFrame()
    output["Actual Values"] = target
    output["Predicted Values"] = predictions
    output.to_csv(
        f"data/predictions_{args.model}_{args.feat}.csv", index=False)

    evaluators = [
        ("R-squared", r2_score),
        ("MAE", mean_absolute_error),
        ("RMSE", root_mean_squared_error)
    ]
    table = PrettyTable()

    for name, evaluator in evaluators:
        score = evaluator(target, predictions)
        table.add_column(name, [round(score, 4)])
    print(table)

    if args.plot:

        # ACTUAL VS. PREDICTED VALUES &
        # RESIDUALS VS. PREDICTED VALUES

        fig, axs = plt.subplots(
            ncols=2, figsize=(8, 4), tight_layout=True)
        axs = np.ravel(axs)

        PredictionErrorDisplay.from_predictions(
            target,
            predictions,
            kind="actual_vs_predicted",
            subsample=100,
            ax=axs[0],
            random_state=0
        )
        axs[0].set_title("Actual vs. Predicted values")

        PredictionErrorDisplay.from_predictions(
            target,
            predictions,
            kind="residual_vs_predicted",
            subsample=100,
            ax=axs[1],
            random_state=0
        )
        axs[1].set_title("Residuals vs. Predicted values")
        axs[1].set(ylim=(-0.3, 0.3))

        plt.suptitle(f"Prediction Scatter Plot of {args.model}")
        plt.savefig(f"figures/overall/plot_{args.model}_{args.feat}.png")
        plt.clf()
        plt.close()

        # ERROR DISTRIBUTION

        plt.subplots(figsize=(4.8, 3.6), tight_layout=True)
        errors = np.subtract(target, predictions)
        errors_mean = np.mean(errors)
        errors_std = np.std(errors)

        x = np.linspace(errors_mean - 3 * errors_std,
                        errors_mean + 3 * errors_std, 100)
        density = stats.gaussian_kde(errors)

        plt.axvline(x=errors_mean, linestyle="dashed", linewidth=1)
        plt.axvline(x=errors_mean - 2 * errors_std,
                    linestyle="dashed", linewidth=1)
        plt.axvline(x=errors_mean + 2 * errors_std,
                    linestyle="dashed", linewidth=1)
        plt.hist(errors, bins=10, edgecolor="black",
                 linewidth=0.8, density=True)
        plt.plot(x, density(x))
        plt.xlim((-0.2, 0.2))
        plt.ylim((0, 50))
        plt.xlabel("Error")
        plt.ylabel("Count")
        plt.title(f"Error distribution of {args.model}")
        plt.savefig(f"figures/overall/errordist_{args.model}_{args.feat}.png")
        plt.clf()
        plt.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--model", type=str, required=True)
    parser.add_argument("--feat", type=str, default="default")
    parser.add_argument("--plot", action=argparse.BooleanOptionalAction)
    args = parser.parse_args()

    with initialize(config_path="../config", version_base=None):
        config = compose(config_name="main")
        evaluate(config, args)
