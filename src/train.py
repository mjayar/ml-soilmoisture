from hydra import compose, initialize
from omegaconf import DictConfig


import argparse
import joblib
import json
import numpy as np
import os
import pandas as pd
from datetime import datetime
from matplotlib import pyplot as plt
from prettytable import PrettyTable
from scipy import stats
from sklearn.metrics import (
    mean_absolute_error,
    root_mean_squared_error,
    r2_score,
    PredictionErrorDisplay
)
from sklearn.model_selection import train_test_split
from sklearn.inspection import permutation_importance
from sklearn.ensemble import ExtraTreesRegressor, RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor, ExtraTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR


def train(config: DictConfig, args: argparse.Namespace):
    assert os.path.isfile(config.data.raw), f"CSV not found: {config.data.raw}"
    data = pd.read_csv(config.data.raw)

    now = datetime.now()
    suffix = f"{now:%Y%m%d%H%M}"

    if args.feat == "gsv":
        target = data.iloc[:, 0].values
        features = data.iloc[:, 1:2].values
    elif args.feat == "ltv":
        target = data.iloc[206:, 0].values
        features = data.iloc[206:, 2:3].values
    elif args.feat == "dtv":
        target = data.iloc[:206, 0].values
        features = data.iloc[:206, 3:4].values
    elif args.feat == "hsv":
        target = data.iloc[:, 0].values
        features = data.iloc[:, 2:7].values
    else:
        target = data.iloc[:, 0].values
        features = data.iloc[:, 1:4].values

    x_train, x_test, y_train, y_test = train_test_split(
        features, target,
        test_size=float(config.train.test_size),
        random_state=int(config.train.random_state),
        shuffle=bool(config.train.shuffle)
    )

    regressors = [
        ("DecisionTree", DecisionTreeRegressor()),
        ("ExtraTrees", ExtraTreesRegressor()),
        ("RandomForest", RandomForestRegressor()),
        ("KNeighbors", KNeighborsRegressor()),
        ("SVR", SVR()),
    ]
    algorithms = [name for name, _ in regressors]

    for name, regressor in regressors:
        regressor.fit(x_train, y_train)

        if args.dump:
            joblib.dump(regressor, f"models/{name}_{args.feat}.sav")

    predictions = np.vstack([regressor.predict(x_test)
                            for _, regressor in regressors])

    evaluators = [
        ("R-squared", r2_score),
        ("MAE", mean_absolute_error),
        ("RMSE", root_mean_squared_error)
    ]
    scores = []

    table = PrettyTable()
    table.add_column("Algorithms", algorithms, align="l")

    for i, (name, evaluator) in enumerate(evaluators):
        score = []

        for pred in predictions:
            score.append(evaluator(y_test, pred))

        table.add_column(name, ['%.4f' % i for i in score])
        scores.append(score)

    print("Evaluation results:")
    print(table)
    print("")

    list_importance_mean = []
    list_importance_std = []

    for name, regressor in regressors:
        importance = permutation_importance(regressor, x_test, y_test)
        importance_mean = importance.importances_mean
        importance_std = importance.importances_std

        importance_meansum = np.sum(importance_mean)
        importance_percentage = importance_mean / importance_meansum * 100

        importance_table = PrettyTable()
        importance_table.add_column("Features", np.arange(
            1, len(importance_mean)+1), align="l")
        importance_table.add_column(
            "Mean", ['%.3f' % i for i in importance_mean])
        importance_table.add_column(
            "Std", ['%.3f' % i for i in importance_std])
        importance_table.add_column(
            "%", ['%.1f' % i for i in importance_percentage], align="r")

        print(f"Feature importance for {name}:")
        print(importance_table)
        print("")

        list_importance_mean.append(importance_mean)
        list_importance_std.append(importance_std)

    if args.plot == "all" or args.plot == "importance":
        fig, axs = plt.subplots(3, 2, figsize=(6, 8), tight_layout=True)
        axs = np.ravel(axs)

        for i, (name, algorithm) in enumerate(algorithms):
            x = np.arange(1, len(importance_mean)+1)
            axs[i].bar(x, list_importance_mean[i], yerr=list_importance_std[i])
            axs[i].set_xticks(x)
            axs[i].set(xlabel="Features", ylabel="Mean Feature Importance",
                       title=algorithms[i])

        fig.suptitle("Feature Importance")
        plt.savefig(f"figures/importance_{suffix}.png")
        plt.clf()

    if args.plot == "all" or args.plot == "predictions":
        colors = ["g", "b", "c", "m", "y", "orange"]
        total = len(predictions[0])
        x = np.linspace(1, total, total)

        fig, axs = plt.subplots(figsize=(8, 4), tight_layout=True)
        axs.plot(x, y_test, color="r",  label="Actual")

        for i, pred in enumerate(predictions):
            axs.plot(x, pred, color=colors[i], label=algorithms[i])

        axs.set(xlabel="Test samples", ylabel="Soil moisture content")
        fig.suptitle("Predictions")
        plt.legend(loc="upper right")
        plt.savefig(f"figures/predictions_{suffix}.png")
        plt.clf()

        for i, pred in enumerate(predictions):
            fig, axs = plt.subplots(ncols=2, figsize=(8, 4), tight_layout=True)
            axs = np.ravel(axs)

            PredictionErrorDisplay.from_predictions(
                y_test, pred,
                kind="actual_vs_predicted",
                subsample=100,
                ax=axs[0],
                random_state=0
            )
            axs[0].set_title("Actual vs. Predicted values")

            PredictionErrorDisplay.from_predictions(
                y_test, pred,
                kind="residual_vs_predicted",
                subsample=100,
                ax=axs[1],
                random_state=0
            )
            axs[1].set_title("Residuals vs. Predicted values")

            plt.suptitle(f"Prediction Scatter Plot of {algorithms[i]}")
            plt.savefig(f"figures/plot_{algorithms[i]}_{suffix}.png")
            plt.clf()

        fig, axs = plt.subplots(3, 2, figsize=(9, 13), tight_layout=True)
        axs = np.ravel(axs)

        for i, pred in enumerate(predictions):
            PredictionErrorDisplay.from_predictions(
                y_test, pred,
                kind="actual_vs_predicted",
                ax=axs[i],
                scatter_kwargs={"alpha": 0.4, "color": "tab:blue"},
                line_kwargs={"color": "tab:red"},
            )
            axs[i].set_title(algorithms[i])
            for j, (name, _) in enumerate(evaluators):
                axs[i].plot([], [], " ", label=f"{name}: {scores[j][i]:.4f}")
            axs[i].legend(loc="upper left")

        fig.suptitle("Prediction Scatter Plot")
        plt.savefig(f"figures/comparison_{suffix}.png")
        plt.clf()

    if args.plot == "all" or args.plot == "errors":
        # Error distribution (violin plot)
        fig, axs = plt.subplots(figsize=(8, 4), tight_layout=True)
        errors = [np.subtract(y_test, pred) for pred in predictions]
        axs.violinplot(errors[::-1], vert=False, showmeans=True)
        axs.set_yticks(np.arange(1, len(algorithms) + 1))
        axs.set_yticklabels(algorithms[::-1])
        axs.set_ylabel("Machine Learning Models")
        axs.set_xlabel("Error")
        fig.suptitle("Error Distribution")
        plt.savefig(f"figures/errorviolin_{suffix}.png")
        plt.clf()
        plt.close()

        # Error distribution plot
        fig, axs = plt.subplots(3, 2, figsize=(9, 10), tight_layout=True)
        axs = np.ravel(axs)

        for i, prediction in enumerate(predictions):
            errors = np.subtract(y_test, prediction)
            errors_mean = np.mean(errors)
            errors_std = np.std(errors)

            x = np.linspace(errors_mean - 3 * errors_std,
                            errors_mean + 3 * errors_std, 100)
            density = stats.gaussian_kde(errors)

            axs[i].axvline(x=errors_mean, linestyle="dashed", linewidth=1)
            axs[i].axvline(x=errors_mean - 2 * errors_std,
                           linestyle="dashed", linewidth=1)
            axs[i].axvline(x=errors_mean + 2 * errors_std,
                           linestyle="dashed", linewidth=1)
            axs[i].hist(errors, bins=10, edgecolor="black",
                        linewidth=0.8, density=True)[1]
            axs[i].plot(x, density(x))
            axs[i].set(xlabel="Error", ylabel="Count")
            axs[i].set_title(algorithms[i])
            axs[i].plot([], [], " ", label=f"Mean: {errors_mean:.4f}")
            axs[i].plot([], [], " ", label=f"Std: {errors_std:.4f}")
            axs[i].legend(loc="upper left")

        fig.suptitle("Error distribution")
        plt.savefig(f"figures/errordist_{suffix}.png")
        plt.clf()
        plt.close()

    if args.plot == "all" or args.plot == "evaluation":
        fig, axs = plt.subplots(2, 2, figsize=(8, 9), tight_layout=True)
        axs = np.ravel(axs)
        for i, score in enumerate(scores):
            axs[i].bar(["DT", "ET", "ETs", "RF", "KN", "SVR"], score)
            axs[i].set(xlabel="Machine Learning Algorithms",
                       ylabel=[name for name, _ in evaluators][i])

        fig.suptitle("Evaluation Metrics")
        plt.savefig(f"figures/evaluation_{suffix}.png")
        plt.clf()
        plt.close()

    results = {
        "y_test": y_test.tolist(),
        "predictions": predictions.tolist(),
        "scores": scores,
        "importance_mean": np.array(list_importance_mean).tolist(),
        "importance_std": np.array(list_importance_std).tolist()
    }

    with open(f"data/results_{args.feat}.json", "w") as f:
        json.dump(results, f)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--feat", type=str, default="default")
    parser.add_argument("--dump", action=argparse.BooleanOptionalAction)
    parser.add_argument("--plot", type=str, default=None)
    args = parser.parse_args()

    with initialize(config_path="../config", version_base=None):
        config = compose(config_name="main")
        train(config, args)
