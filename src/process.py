from hydra import compose, initialize
from omegaconf import DictConfig

import argparse
import cv2 as cv
import numpy as np
import os
import pandas as pd
from datetime import datetime
from glob import glob

from utils.image import non_zero_pixels_percentage


def process(config: DictConfig, args: argparse.Namespace):
    if os.path.isfile(config.data.raw):
        data = pd.read_csv(config.data.raw)
    else:
        data = pd.DataFrame()

    gsv = np.array([])
    ltv = np.array([])
    dtv = np.array([])
    hue = np.array([])
    sat = np.array([])
    val = np.array([])

    time_start = datetime.now()
    print(f"{time_start:%H:%M:%S} - Processing images...")

    imgs = glob(os.path.join(config.images.raw, "*.jpg"))
    imgs_len = len(imgs)

    for i, img_path in enumerate(imgs):
        img_filename, ext = os.path.splitext(os.path.basename(img_path))
        progress = round(i / imgs_len * 100, 1)
        print(f"[{progress}%] Processing "
              f"{img_filename}...", end="\r", flush=True)

        img = cv.imread(img_path)
        img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

        mean = np.mean(img_gray)
        gsv = np.append(gsv, mean)

        img_light = cv.threshold(img_gray, int(config.process.threshold),
                                 int(config.process.max_threshold), cv.THRESH_BINARY)[1]
        if bool(config.process.dilate):
            img_light = cv.dilate(img_light, kernel=np.ones(
                (10, 10), np.uint8), iterations=1)
        val_light = non_zero_pixels_percentage(img_light)
        ltv = np.append(ltv, val_light)

        img_neg = cv.bitwise_not(img_gray)
        img_dark = cv.threshold(img_neg, int(config.process.threshold),
                                int(config.process.max_threshold), cv.THRESH_BINARY)[1]
        if bool(config.process.dilate):
            img_dark = cv.dilate(img_dark, kernel=np.ones(
                (10, 10), np.uint8), iterations=1)
        val_dark = non_zero_pixels_percentage(img_dark)
        dtv = np.append(dtv, val_dark)

        hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
        hsv_mean = cv.mean(hsv)
        hue = np.append(hue, [hsv_mean[0]])
        sat = np.append(sat, [hsv_mean[1]])
        val = np.append(val, [hsv_mean[2]])

        if args.dump:
            if not os.path.exists(config.images.processed):
                os.makedirs(config.images.processed)
            cv.imwrite(os.path.join(config.images.processed,
                       f"{img_filename}_gray.jpg"), img_gray)
            cv.imwrite(os.path.join(config.images.processed,
                       f"{img_filename}_negative.jpg"), img_neg)
            cv.imwrite(os.path.join(config.images.processed,
                       f"{img_filename}_light_thresholded.jpg"), img_light)
            cv.imwrite(os.path.join(config.images.processed,
                       f"{img_filename}_dark_thresholded.jpg"), img_dark)
    time_end = datetime.now()
    print(f"[100%] Done! Total time: {time_end - time_start}")

    data["Image-grayscale values"] = gsv
    data["Light thresholded values"] = ltv
    data["Dark thresholded values"] = dtv
    data["Hue"] = hue
    data["Saturation"] = sat
    data["Value"] = val
    data.to_csv(config.data.raw, index=False)
    sorted_data = data.sort_values(by="Water content")
    sorted_data.to_csv(config.data.sorted, index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dump", action=argparse.BooleanOptionalAction)
    args = parser.parse_args()

    with initialize(config_path="../config", version_base=None):
        config = compose(config_name="main")
        process(config, args)
