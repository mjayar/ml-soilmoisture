import argparse
import json
import numpy as np
import scipy.stats as stats
from prettytable import PrettyTable
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor
from sklearn.metrics import (
    r2_score,
    mean_absolute_error,
    root_mean_squared_error
)
from utils.plot import prediction_error_display


def detect_outliers(options: argparse.Namespace):
    algorithms = [
        "Decision Tree",
        "Extra Trees",
        "Random Forest",
        "K-Neighbors",
        "SVR"
    ]
    evaluators = [
        ("R2", r2_score),
        ("MAE", mean_absolute_error),
        ("RMSE", root_mean_squared_error)
    ]

    results = None

    with open(f'data/results_{options.feat}.json', 'r') as f:
        results = json.load(f)

    y_test = np.array(results["y_test"])
    predictions = np.array(results["predictions"])

    sw_table = PrettyTable()
    sw_table.field_names = ['', 'W', 'p-value']

    for i, pred in enumerate(predictions):
        errors = np.subtract(y_test, pred)
        w = stats.shapiro(errors)

        sw_table.add_row([
            algorithms[i],
            '{:.4f}'.format(w.statistic),
            '{:.3e}'.format(w.pvalue)
        ])

    print('Shapiro-Wilk test:')
    print(sw_table)

    zscore_table = PrettyTable()
    zscore_table.field_names = ['', 'R2', 'MAE', 'RMSE']

    for i, pred in enumerate(predictions):
        errors = np.subtract(y_test, pred)
        zscore = stats.zscore(errors)
        abs_zscore = np.abs(zscore)
        nonoutliers = abs_zscore < 3
        scores = [evaluator(y_test[nonoutliers], pred[nonoutliers])
                  for _, evaluator in evaluators]
        zscore_table.add_row([algorithms[i]] +
                             ['{:.4f}'.format(i) for i in scores])

        if options.plot:
            prediction_error_display(
                y_test[nonoutliers],
                pred[nonoutliers],
                suptitle=f"Prediction Scatter Plot of {algorithms[i]}",
                filepath=f"figures/plot_zscore_{algorithms[i]}.png"
            )

    print('Z-Score test:')
    print(zscore_table)

    iqr_table = PrettyTable()
    iqr_table.field_names = ['', 'R2', 'MAE', 'RMSE']

    for i, pred in enumerate(predictions):
        errors = np.abs(np.subtract(y_test, pred))
        q1 = np.quantile(errors, 0.25)
        q3 = np.quantile(errors, 0.75)
        iqr = q3-q1
        nonoutliers = (errors >= q1-1.5*iqr) & (errors <= q3+1.5*iqr)

        scores = [evaluator(y_test[nonoutliers], pred[nonoutliers])
                  for _, evaluator in evaluators]
        iqr_table.add_row([algorithms[i]] +
                          ['{:.4f}'.format(i) for i in scores])

        if options.plot:
            prediction_error_display(
                y_test[nonoutliers],
                pred[nonoutliers],
                suptitle=f"Prediction Scatter Plot of {algorithms[i]}",
                filepath=f"figures/plot_iqr_{algorithms[i]}.png"
            )

    print('IQR test:')
    print(iqr_table)

    isoforest_table = PrettyTable()
    isoforest_table.field_names = ['', 'R2', 'MAE', 'RMSE']

    outliers_table = PrettyTable()
    outliers_table.field_names = ['', 'Actual values', 'Predicted values']

    for i, pred in enumerate(predictions):
        errors = np.subtract(y_test, pred)
        points = np.array([y_test, errors]).T

        isoforest = IsolationForest(
            random_state=0,
            warm_start=True,
            contamination=0.05
        )
        isoforest.fit(points)
        nonoutliers = ((isoforest.predict(points)+1)//2).astype(bool)

        scores = [evaluator(y_test[nonoutliers], pred[nonoutliers])
                  for _, evaluator in evaluators]
        isoforest_table.add_row([algorithms[i]] +
                                ['{:.4f}'.format(i) for i in scores])

        if options.plot:
            prediction_error_display(
                y_test[nonoutliers],
                pred[nonoutliers],
                suptitle=f"Prediction Scatter Plot of {algorithms[i]}",
                filepath=f"figures/plot_isoforest_{algorithms[i]}.png"
            )

        outliers = ~nonoutliers
        for j in range(len(y_test[outliers])):
            outliers_table.add_row(
                [algorithms[i]] + [
                    '{:.4f}'.format(y_test[outliers][j]),
                    '{:.4f}'.format(pred[outliers][j])
                ])

    print('Isolation Forest outliers:')
    print(outliers_table)

    print('Isolation Forest test:')
    print(isoforest_table)

    lof_table = PrettyTable()
    lof_table.field_names = ['', 'R2', 'MAE', 'RMSE']

    for i, pred in enumerate(predictions):
        errors = np.subtract(y_test, pred)
        points = np.array([y_test, errors]).T

        lof = LocalOutlierFactor()
        nonoutliers = ((lof.fit_predict(points)+1)//2).astype(bool)

        scores = [evaluator(y_test[nonoutliers], pred[nonoutliers])
                  for _, evaluator in evaluators]
        lof_table.add_row([algorithms[i]] +
                          ['{:.4f}'.format(i) for i in scores])

        if options.plot:
            prediction_error_display(
                y_test[nonoutliers],
                pred[nonoutliers],
                suptitle=f"Prediction Scatter Plot of {algorithms[i]}",
                filepath=f"figures/plot_lof_{algorithms[i]}.png"
            )

    print('LOF test:')
    print(lof_table)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--feat', type=str, default='default')
    parser.add_argument('--plot', action=argparse.BooleanOptionalAction)

    options = parser.parse_args()
    detect_outliers(options)
