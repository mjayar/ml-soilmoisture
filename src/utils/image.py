import cv2 as cv
from cv2.typing import MatLike


def crop_center(img: MatLike, size: tuple[float, float]) -> MatLike:
    height, width = img.shape[:2]
    x, y = size
    cx, cy = width//2, height//2
    return img[cy - y//2: cy + y//2, cx - x//2: cx + x//2]


def non_zero_pixels_percentage(img: MatLike) -> float:
    height, width = img.shape[:2]
    non_zero_pixels = cv.countNonZero(img)
    total_pixels = height * width
    return non_zero_pixels / total_pixels
