import matplotlib.pyplot as plt
import numpy as np
from numpy.typing import ArrayLike
from scipy import stats
from scipy.optimize import curve_fit
from sklearn.metrics import PredictionErrorDisplay
from typing import Any


def error_distribution_plot(
    errors: ArrayLike,
    *,
    title: str,
    filepath: str
) -> None:
    mean = np.mean(errors)
    std = np.std(errors)

    density = stats.gaussian_kde(errors)
    x = np.linspace(mean - 3*std, mean + 3*std, 100)

    plt.subplots(figsize=(4.8, 3.6), tight_layout=True)
    plt.axvline(x=mean, linestyle="dashed", linewidth=1)
    plt.axvline(x=mean - 2*std, linestyle="dashed", linewidth=1)
    plt.axvline(x=mean + 2*std, linestyle="dashed", linewidth=1)
    plt.hist(errors, bins=10, edgecolor="black", linewidth=0.8, density=True)
    plt.plot(x, density(x))
    plt.xlim((-0.2, 0.2))
    plt.ylim((0, 50))
    plt.xlabel("Error")
    plt.ylabel("Count")
    plt.plot([], [], " ", label=f"Mean: {mean:.4f}")
    plt.plot([], [], " ", label=f"Std: {std:.4f}")
    plt.legend(loc="upper left")
    plt.title(title)
    plt.savefig(filepath)
    plt.close("all")


def feature_values_plot(
    x: ArrayLike,
    y: ArrayLike,
    px: ArrayLike,
    py: ArrayLike,
    *,
    fit: Any,
    title: str,
    filepath: str,
) -> None:
    ux = np.unique(x)
    popt, _ = curve_fit(fit, x, y)
    plt.subplots(figsize=(6.4, 4.8), tight_layout=True)
    plt.scatter(px, py)
    plt.plot(ux, fit(ux, *popt), 'r--')
    plt.xlabel("Water content")
    plt.ylabel(title)
    plt.plot([], [], "r--", label="Trend")
    plt.plot([], [], "o", label="Values")
    plt.legend(loc="upper right")
    plt.title(title)
    plt.savefig(filepath)
    plt.close("all")


def pfi_bar_plot(
    mean: ArrayLike,
    std: ArrayLike,
    *,
    title: str,
    filepath: str,
) -> None:
    plt.subplots(figsize=(4.8, 3.6), tight_layout=True)
    x = np.arange(1, len(mean)+1)
    plt.bar(x, mean, yerr=std)
    plt.xticks(x)
    plt.ylim((0, 1.8))
    plt.xlabel("Features")
    plt.ylabel("Mean Feature Importance")
    plt.title(title)
    plt.savefig(filepath)
    plt.close("all")


def prediction_error_display(
    y_true: ArrayLike,
    y_pred: ArrayLike,
    *,
    suptitle: str,
    filepath: str
) -> None:
    fig, (ax1, ax2) = plt.subplots(ncols=2, figsize=(8, 4), tight_layout=True)

    PredictionErrorDisplay.from_predictions(
        y_true * 100,
        y_pred * 100,
        kind="actual_vs_predicted",
        ax=ax1,
        random_state=0
    )

    PredictionErrorDisplay.from_predictions(
        y_true * 100,
        y_pred * 100,
        kind="residual_vs_predicted",
        ax=ax2,
        random_state=0
    )

    ax1.set_title("Actual vs. Predicted values")
    ax2.set_title("Residuals vs. Predicted values")
    ax2.set(ylim=(-30, 30))

    fig.suptitle(suptitle)
    plt.savefig(filepath)
    plt.close("all")
