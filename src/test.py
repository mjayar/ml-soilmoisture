from hydra import compose, initialize
from omegaconf import DictConfig

import argparse
import cv2 as cv
import numpy as np
import os
import pandas as pd
from datetime import datetime
from glob import glob
from matplotlib import pyplot as plt
from prettytable import PrettyTable
from scipy import stats
from sklearn.metrics import (
    mean_absolute_error,
    mean_squared_error,
    root_mean_squared_error,
    r2_score,
    PredictionErrorDisplay
)
from sklearn.model_selection import train_test_split
from sklearn.inspection import permutation_importance
from sklearn.ensemble import ExtraTreesRegressor, RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor, ExtraTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR


from utils.image import crop_center, non_zero_pixels_percentage


def process(config: DictConfig, args: argparse.Namespace):
    if os.path.isfile(config.data.raw):
        data = pd.read_csv(config.data.raw)
    else:
        data = pd.DataFrame()

    values = {
        "gray": np.array([]),
        "light": np.array([]),
        "dark": np.array([]),
        "hue": np.array([]),
        "sat": np.array([]),
        "val": np.array([]),
    }

    time_start = datetime.now()

    imgs = glob(os.path.join(config.images.raw, "*.jpg"))
    imgs_len = len(imgs)

    for i, img_path in enumerate(imgs):
        img_filename, ext = os.path.splitext(os.path.basename(img_path))
        progress = round(i / imgs_len * 100, 1)
        print(f"[{progress}%] Processing {
              img_filename}...            ", end="\r", flush=True)

        img = cv.imread(img_path)
        img = crop_center(img, (2000, 2000))
        img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

        mean = cv.mean(img_gray)[0]
        values["gray"] = np.append(values["gray"], mean)

        img_light = cv.threshold(img_gray, int(config.process.threshold),
                                 int(config.process.max_threshold), cv.THRESH_BINARY)[1]
        if bool(config.process.dilate):
            img_light = cv.dilate(img_light, kernel=np.ones(
                (10, 10), np.uint8), iterations=1)
        val_light = non_zero_pixels_percentage(img_light)
        values["light"] = np.append(values["light"], val_light)

        img_neg = cv.bitwise_not(img_gray)
        img_dark = cv.threshold(img_neg, int(config.process.threshold),
                                int(config.process.max_threshold), cv.THRESH_BINARY)[1]
        if bool(config.process.dilate):
            img_dark = cv.dilate(img_dark, kernel=np.ones(
                (10, 10), np.uint8), iterations=1)
        val_dark = non_zero_pixels_percentage(img_dark)
        values["dark"] = np.append(values["dark"], val_dark)

        hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
        hsv_mean = cv.mean(hsv)
        values["hue"] = np.append(values["hue"], [hsv_mean[0]])
        values["sat"] = np.append(values["sat"], [hsv_mean[1]])
        values["val"] = np.append(values["val"], [hsv_mean[2]])

        # if args.dump:
        #     if not os.path.exists(config.images.processed):
        #         os.makedirs(config.images.processed)
        #     cv.imwrite(os.path.join(config.images.processed,
        #                f"{img_filename}_gray.jpg"), img_gray)
        #     cv.imwrite(os.path.join(config.images.processed,
        #                f"{img_filename}_light_thresholded.jpg"), img_light)
        #     cv.imwrite(os.path.join(config.images.processed,
        #                f"{img_filename}_dark_thresholded.jpg"), img_dark)
    time_end = datetime.now()
    print(f"[100%] Done! Total time: {time_end - time_start}")

    data["Image-grayscale values"] = values["gray"]
    data["Light thresholded values"] = values["light"]
    data["Dark thresholded values"] = values["dark"]
    data["Hue"] = values["hue"]
    data["Saturation"] = values["sat"]
    data["Value"] = values["val"]
    return data


def train(config: DictConfig, args: argparse.Namespace, data: pd.DataFrame):
    target = data.iloc[:, 0].values
    if args.hsv:
        features = data.iloc[:, 2:7].values
    else:
        features = data.iloc[:, 1:4].values

    x_train, x_test, y_train, y_test = train_test_split(
        features, target,
        test_size=float(config.train.test_size),
        random_state=int(config.train.random_state),
        shuffle=bool(config.train.shuffle)
    )

    algorithms = {
        "DecisionTree": DecisionTreeRegressor(),
        "ExtraTree": ExtraTreeRegressor(),
        "ExtraTrees": ExtraTreesRegressor(),
        "RandomForest": RandomForestRegressor(),
        "KNeighbors": KNeighborsRegressor(),
        "SVR": SVR(),
    }

    for name, algorithm in algorithms.items():
        algorithm.fit(x_train, y_train)

        # if args.dump:
        #     joblib.dump(algorithm, os.path.join("models", f"{name}.sav"))

    predictions = np.vstack([model.predict(x_test)
                            for model in algorithms.values()])

    evaluators = {
        "R-squared": r2_score,
        "MAE": mean_absolute_error,
        "MSE": mean_squared_error,
        "RMSE": root_mean_squared_error
    }
    scores = []

    evaluation_table = PrettyTable()
    evaluation_table.add_column("Algorithms", list(algorithms), align="l")

    for i, (name, evaluator) in enumerate(evaluators.items()):
        score = []

        for pred in predictions:
            score.append(evaluator(y_test, pred))

        evaluation_table.add_column(name, ['%.4f' % i for i in score])
        scores.append(score)

    print("Evaluation results:")
    print(evaluation_table)
    print("")

    if args.hsv:
        feature_labels = ["Light thresholded",
                          "Dark thresholded",
                          "Hue",
                          "Saturation",
                          "Value"]
    else:
        feature_labels = ["Image grayscale",
                          "Light thresholded",
                          "Dark thresholded"]

    list_importance_mean = []
    list_importance_std = []

    for name, regressor in algorithms.items():
        importance = permutation_importance(regressor, x_test, y_test)
        importance_mean = importance.importances_mean
        importance_std = importance.importances_std

        importance_meansum = np.sum(importance_mean)
        importance_percentage = importance_mean / importance_meansum * 100

        importance_table = PrettyTable()
        importance_table.add_column("Features", feature_labels, align="l")
        importance_table.add_column(
            "Mean", ['%.3f' % i for i in importance_mean])
        importance_table.add_column(
            "Std", ['%.3f' % i for i in importance_std])
        importance_table.add_column(
            "%", ['%.1f' % i for i in importance_percentage], align="r")

        print(f"Feature importance for {name}:")
        print(importance_table)
        print("")

        list_importance_mean.append(importance_mean)
        list_importance_std.append(importance_std)

    if args.plot == "all" or args.plot == "importance":
        fig, axs = plt.subplots(3, 2, figsize=(6, 8), tight_layout=True)
        axs = np.ravel(axs)

        for i, (name, regressor) in enumerate(algorithms.items()):
            axs[i].bar(["LTV", "DTV", "H", "S", "V"] if args.hsv else [
                       "GSV", "LTV", "DTV"], list_importance_mean[i], yerr=list_importance_std[i])
            axs[i].set(xlabel="Features", ylabel="Mean Feature Importance",
                       title=list(algorithms)[i])

        fig.suptitle("Feature Importance")
        plt.savefig(os.path.join("figures/test", "importance.png"))
        plt.clf()

    if args.plot == "all" or args.plot == "predictions":
        colors = ["g", "b", "c", "m", "y", "orange"]
        total = len(predictions[0])
        x = np.linspace(1, total, total)

        fig, axs = plt.subplots(figsize=(8, 4), tight_layout=True)
        axs.plot(x, y_test, color="r",  label="Actual")

        for i, pred in enumerate(predictions):
            axs.plot(x, pred, color=colors[i], label=list(algorithms)[i])

        axs.set(xlabel="Test samples", ylabel="Soil moisture content")
        fig.suptitle("Predictions Plot")
        plt.legend(loc="upper right")
        plt.savefig(os.path.join("figures/test", "predictions.png"))
        plt.clf()

        for i, pred in enumerate(predictions):
            fig, axs = plt.subplots(ncols=2, figsize=(8, 4), tight_layout=True)
            axs = np.ravel(axs)

            PredictionErrorDisplay.from_predictions(
                y_test, pred,
                kind="actual_vs_predicted",
                subsample=100,
                ax=axs[0],
                random_state=0
            )
            axs[0].set_title("Actual vs. Predicted values")

            PredictionErrorDisplay.from_predictions(
                y_test, pred,
                kind="residual_vs_predicted",
                subsample=100,
                ax=axs[1],
                random_state=0
            )
            axs[1].set_title("Residuals vs. Predicted values")

            plt.suptitle(f"Prediction Scatter Plot of {list(algorithms)[i]}")
            plt.savefig(os.path.join(
                "figures", f"plot_{list(algorithms)[i]}.png"))
            plt.clf()

        fig, axs = plt.subplots(3, 2, figsize=(9, 13), tight_layout=True)
        axs = np.ravel(axs)

        for i, pred in enumerate(predictions):
            PredictionErrorDisplay.from_predictions(
                y_test, pred,
                kind="actual_vs_predicted",
                ax=axs[i],
                scatter_kwargs={"alpha": 0.4, "color": "tab:blue"},
                line_kwargs={"color": "tab:red"},
            )
            axs[i].set_title(list(algorithms)[i])
            for j, name in enumerate(evaluators):
                axs[i].plot([], [], " ", label=f"{name}: {scores[j][i]:.4f}")
            axs[i].legend(loc="upper left")

        fig.suptitle("Machine Learning Models Prediction Scatter Plot")
        plt.savefig(os.path.join("figures/test", "comparison.png"))
        plt.clf()

    if args.plot == "all" or args.plot == "errors":
        # Error distribution (violin plot)
        fig, axs = plt.subplots(figsize=(8, 4), tight_layout=True)
        errors = [np.subtract(y_test, pred) for pred in predictions]
        axs.violinplot(errors[::-1], vert=False, showmeans=True)
        axs.set_yticks(np.arange(1, len(algorithms) + 1))
        axs.set_yticklabels(list(algorithms)[::-1])
        axs.set_ylabel("Machine Learning Models")
        axs.set_xlabel("Error")
        fig.suptitle("Machine Learning Models Error Distribution")
        plt.savefig(os.path.join("figures/test", "errorviolin.png"))
        plt.clf()

        # Error distribution plot
        fig, axs = plt.subplots(3, 2, figsize=(9, 10), tight_layout=True)
        axs = np.ravel(axs)

        for i, prediction in enumerate(predictions):
            errors = np.subtract(y_test, prediction)
            errors_mean = np.mean(errors)
            errors_std = np.std(errors)

            x = np.linspace(errors_mean - 3 * errors_std,
                            errors_mean + 3 * errors_std, 100)
            density = stats.gaussian_kde(errors)

            axs[i].axvline(x=errors_mean, linestyle="dashed", linewidth=1)
            axs[i].axvline(x=errors_mean - 2 * errors_std,
                           linestyle="dashed", linewidth=1)
            axs[i].axvline(x=errors_mean + 2 * errors_std,
                           linestyle="dashed", linewidth=1)
            axs[i].hist(errors, bins=10, edgecolor="black",
                        linewidth=0.8, density=True)[1]
            axs[i].plot(x, density(x))
            axs[i].set(xlabel="Error", ylabel="Count")
            axs[i].set_title(list(algorithms)[i])
            axs[i].plot([], [], " ", label=f"Mean: {errors_mean:.4f}")
            axs[i].plot([], [], " ", label=f"Std: {errors_std:.4f}")
            axs[i].legend(loc="upper left")

        fig.suptitle("Machine Learning Models Error distribution")
        plt.savefig(os.path.join("figures/test", "errordist.png"))
        plt.clf()

    if args.plot == "all" or args.plot == "evaluation":
        fig, axs = plt.subplots(2, 2, figsize=(8, 9), tight_layout=True)
        axs = np.ravel(axs)
        for i, score in enumerate(scores):
            axs[i].bar(["DT", "ET", "ETs", "RF", "KN", "SVR"], score)
            axs[i].set(xlabel="Machine Learning Algorithms",
                       ylabel=list(evaluators)[i])

        fig.suptitle("Machine Learning Models Evaluation")
        plt.savefig(os.path.join("figures/test", "evaluation.png"))
        plt.clf()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Process soil images"
    )
    parser.add_argument("--hsv", action=argparse.BooleanOptionalAction)
    parser.add_argument("--plot", type=str, default=None)
    args = parser.parse_args()

    with initialize(config_path="../config", version_base=None):
        config = compose(config_name="main")
        data = process(config, args)
        train(config, args, data)
