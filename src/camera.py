from hydra import compose, initialize
from omegaconf import DictConfig

import argparse
import cv2 as cv
import joblib
import numpy as np
import os

from utils.image import non_zero_pixels_percentage


def camera(config: DictConfig, args: argparse.Namespace):
    model_path = os.path.join("models", f"{args.model}.sav")
    assert os.path.isfile(model_path), f"Model not found: {model_path}"

    model = joblib.load(model_path)
    cap = cv.VideoCapture(int(args.camera))

    while True:
        frame = cap.read()[1]
        img_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        gray = np.dstack((img_gray, img_gray, img_gray))

        img_light = cv.threshold(img_gray, int(config.process.threshold),
                                 int(config.process.max_threshold), cv.THRESH_BINARY)[1]
        if bool(config.process.dilate):
            img_light = cv.dilate(img_light, kernel=np.ones(
                (10, 10), np.uint8), iterations=1)
        light = np.dstack((img_light, img_light, img_light))

        img_neg = cv.bitwise_not(img_gray)
        img_dark = cv.threshold(img_neg, int(config.process.threshold),
                                int(config.process.max_threshold), cv.THRESH_BINARY)[1]
        if bool(config.process.dilate):
            img_dark = cv.dilate(img_dark, kernel=np.ones(
                (10, 10), np.uint8), iterations=1)
        dark = np.dstack((img_dark, img_dark, img_dark))

        top = np.concatenate((frame, gray), axis=1)
        bot = np.concatenate((light, dark), axis=1)
        screen = np.concatenate((top, bot), axis=0)

        val_gray = cv.mean(img_gray)[0]
        val_light = non_zero_pixels_percentage(img_light)
        val_dark = non_zero_pixels_percentage(img_dark)

        hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        val_hue, val_sat, val_val = cv.mean(hsv)[0:3]

        if args.use_hsv:
            features = np.array(
                [val_light, val_dark, val_hue, val_sat, val_val])
        else:
            features = np.array([val_gray, val_light, val_dark])

        prediction = model.predict(np.array([features]))[0] * 100

        cv.putText(screen, f"GSV: {val_gray:0.2f}", (20, 20),
                   cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
        cv.putText(screen, f"LTV: {val_light:0.6f}", (20, 35),
                   cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
        cv.putText(screen, f"DTV: {val_dark:0.6f}", (20, 50),
                   cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
        cv.putText(screen, f"Prediction: {prediction:0.1f}%", (20, 65),
                   cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
        cv.imshow('Rapid soil moisture content', screen)

        if cv.waitKey(1) & 0xFF == ord("q"):
            break


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Realtime machine learning models evaluation"
    )
    parser.add_argument("--camera", type=int, default=0)
    parser.add_argument("--model", type=str, required=True)
    parser.add_argument("--use-hsv", action=argparse.BooleanOptionalAction)
    args = parser.parse_args()

    with initialize(config_path="../config", version_base=None):
        config = compose(config_name="main")
        camera(config, args)
