from hydra import compose, initialize
from omegaconf import DictConfig

import argparse
import json
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from datetime import datetime
from prettytable import PrettyTable
from scipy import stats
from sklearn.cluster import KMeans
from sklearn.metrics import (
    r2_score,
    mean_absolute_error,
    root_mean_squared_error
)

from utils.plot import (
    error_distribution_plot,
    feature_values_plot,
    pfi_bar_plot,
    prediction_error_display
)


def plot(config: DictConfig, args: argparse.Namespace):
    assert os.path.isfile(config.data.raw), f"File not found: {
        config.data.raw}"
    data = pd.read_csv(config.data.raw)

    now = datetime.now()
    suffix = f"{now:%Y%m%d%H%M}"

    gsv = data["Image-grayscale values"]
    ltv = data["Light thresholded values"]
    dtv = data["Dark thresholded values"]
    wc = data["Water content"]
    ux = np.unique(wc)

    mean_gsv = []
    mean_ltv = []
    mean_dtv = []
    ux3 = np.array([])
    n_clusters = 5

    for i in ux:
        kmeans = KMeans(n_clusters=n_clusters)
        kmeans.fit(np.column_stack(([i]*len(gsv[wc == i]), gsv[wc == i])))

        labels = kmeans.labels_

        for l in labels:
            ux3 = np.append(ux3, i)
            mean_gsv.append(np.mean(gsv[wc == i][labels == l]))
            mean_ltv.append(np.mean(ltv[wc == i][labels == l]))
            mean_dtv.append(np.mean(dtv[wc == i][labels == l]))

    if args.plot == "all" or args.plot == "trend":
        feature_values_plot(
            wc, gsv, ux3, mean_gsv,
            fit=reciprocal,
            title="Image-grayscale values",
            filepath="figures/gsv.png"
        )

        feature_values_plot(
            wc, ltv, ux3, mean_ltv,
            fit=poly4,
            title="Light-thresholded values",
            filepath="figures/ltv.png"
        )

        feature_values_plot(
            wc, dtv, ux3, mean_dtv,
            fit=poly6,
            title="Dark-thresholded values",
            filepath="figures/dtv.png"
        )

    results = None
    results_path = f"data/results_{args.feat}.json"

    assert os.path.isfile(results_path), f"File not found: {results_path}"
    with open(results_path, "r") as f:
        results = json.load(f)

    algorithms = [
        "Decision Tree",
        "Extra Trees",
        "Random Forest",
        "K-Neighbors",
        "SVR"
    ]
    evaluators = [
        ("R2", r2_score),
        ("MAE", mean_absolute_error),
        ("RMSE", root_mean_squared_error)
    ]

    scores = results["scores"]

    table = PrettyTable()
    table.add_column("Algorithms", algorithms, align="l")

    for i, (name, _) in enumerate(evaluators):
        table.add_column(name, ["%.4f" % j for j in scores[i]])
    print(table)

    y_test = np.array(results["y_test"])
    predictions = np.array(results["predictions"])
    importance_mean = np.array(results["importance_mean"])
    importance_std = np.array(results["importance_std"])

    for i, name in enumerate(algorithms):
        table = PrettyTable()
        table.add_column("Features", np.arange(
            1, len(importance_mean[i])+1), align="l")
        table.add_column("Mean", ["%.3f" % j for j in importance_mean[i]])
        table.add_column("Std", ["%.3f" % j for j in importance_std[i]])
        table.add_column("%", ["%.1f" % j for j in (
            100*importance_mean[i]/importance_mean[i].sum())])
        print(f"Feature importance for {name}:")
        print(table)
        print("")

    if args.plot == "all" or args.plot == "result":
        for i, algorithm in enumerate(algorithms):
            pfi_bar_plot(
                importance_mean[i],
                importance_std[i],
                title=f"Mean Feature Importance of {algorithm}",
                filepath=f"figures/importance_{algorithm}_{suffix}.png",
            )

            prediction_error_display(
                y_test,
                predictions[i],
                suptitle=f"Prediction Scatter Plot of {algorithm}",
                filepath=f"figures/plot_{algorithm}_{suffix}.png",
            )

            error_distribution_plot(
                np.subtract(y_test, predictions[i]),
                title=f"Error distribution of {algorithms[i]}",
                filepath=f"figures/errordist_{algorithms[i]}_{suffix}.png"
            )

    list_filtered_ytest = []
    list_filtered_predictions = []

    for i, pred in enumerate(predictions):
        errors = np.abs(np.subtract(y_test, pred))
        z_score = stats.zscore(errors)
        abs_z_score = np.abs(z_score)
        filtered_entries = (abs_z_score < 2.5)
        filtered_ytest = y_test[filtered_entries]
        filtered_prediction = pred[filtered_entries]
        list_filtered_ytest.append(filtered_ytest)
        list_filtered_predictions.append(filtered_prediction)

        if args.plot == "all" or args.plot == "filtered":
            prediction_error_display(
                filtered_ytest,
                filtered_prediction,
                suptitle=f"Prediction Scatter Plot of {algorithms[i]}",
                filepath=f"figures/plot_filtered_{algorithms[i]}_{suffix}.png"
            )

    table = PrettyTable()
    table.add_column("Algorithms", algorithms, align="l")

    for i, (name, evaluator) in enumerate(evaluators):
        score = []
        for i in range(len(list_filtered_predictions)):
            score.append(evaluator(
                list_filtered_ytest[i],
                list_filtered_predictions[i]
            ))
        table.add_column(name, ["%.4f" % i for i in score])
    print(table)


def gaussian(x: float, a: float, b: float, c: float, d: float) -> float:
    return a * np.exp(-(x - b)**2 / (2 * c**2)) + d


def poly3(x: float, a: float, b: float, c: float, d: float) -> float:
    return a * x**3 + b * x**2 + c * x + d


def poly4(x: float, a: float, b: float, c: float, d: float, e: float) -> float:
    return a * x**4 + b * x**3 + c * x**2 + d * x + e


def poly6(x: float, a: float, b: float, c: float, d: float, e: float, f: float) -> float:
    return a * x**6 + b * x**5 + c * x**4 + d * x**3 + e * x**2 + f * x


def reciprocal(x: float, a: float, b: float, c: float, d: float, e: float) -> float:
    return a / (b * x + c) + d * x + e


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--feat", type=str, default="default")
    parser.add_argument("--plot", type=str, default=None)
    args = parser.parse_args()

    with initialize(config_path="../config", version_base=None):
        config = compose(config_name="main")
        plot(config, args)
